import java.io.IOException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;

import javax.mail.Session;
import javax.mail.Store;

public class ReadEmail
{
	private String   IMAP_AUTH_EMAIL;
	private String   IMAP_AUTH_PWD;
	private String   IMAP_Server     = "imap.gmail.com"    ;
	private String   IMAP_Port       = "993"               ;

	private String message = "";

	public ReadEmail(String mail, String pass)
	{
		IMAP_AUTH_EMAIL = mail;
		IMAP_AUTH_PWD = pass;
		Properties properties = new Properties();
		properties.put("mail.debug"           , "false"  );
		properties.put("mail.store.protocol"  , "imaps"  );
		properties.put("mail.imap.ssl.enable" , "true"   );
		properties.put("mail.imap.port"       , IMAP_Port);
        
		Authenticator auth = new EmailAuthenticator(IMAP_AUTH_EMAIL, IMAP_AUTH_PWD);
		Session session = Session.getDefaultInstance(properties, auth);
		session.setDebug(false);
			
		try {
	        Store store = session.getStore();

	        store.connect(IMAP_Server, IMAP_AUTH_EMAIL, IMAP_AUTH_PWD);

	        Folder inbox = store.getFolder("INBOX");

	        inbox.open(Folder.READ_ONLY);
	
	        this.message += "Количество сообщений : " + String.valueOf(inbox.getMessageCount()) + "\n";
	        if (inbox.getMessageCount() == 0)
	        	return;

	        Message message = inbox.getMessage(inbox.getMessageCount());
	        Multipart mp = (Multipart) message.getContent();

	        for (int i = 0; i < mp.getCount(); i++){
		        BodyPart  bp = mp.getBodyPart(i);
		        if (bp.getFileName() == null)
	        		this.message += "Последние сообщение : \n" + bp.getContent() + "\n";
	        	else
					this.message += "Файл : " + bp.getFileName() + "\n";
	        }
		} catch (MessagingException | IOException e) {
			System.err.println(e.getMessage());
		}
	}

	public String getMessage() {
		return message;
	}
}
