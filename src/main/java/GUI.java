import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GUI {

    private static final JFrame MAIN_WINDOW = new JFrame("E-mail client");
    private static final JLabel FROM = new JLabel("From: ");
    private static final JLabel PASS = new JLabel("Password:");
    private static final JLabel TO = new JLabel("To: ");
    private static final JLabel MESSAGE = new JLabel("Message: ");
    private static final JLabel TOPIC = new JLabel("Topic: ");
    private static final JLabel GET_MESSAGE = new JLabel("Get message: ");
    private static final JTextField FROM_FIELD = new JTextField(45);
    private static final JTextField TO_FIELD = new JTextField(45);
    private static final JTextField TOPIC_FIELD = new JTextField(45);
    private static final JPasswordField PASSWORD = new JPasswordField(45);
    private static final JTextArea MESSAGE_AREA = new JTextArea(45, 45);
    private static final JTextArea GET_MESSAGE_AREA = new JTextArea(45, 45);
    private static final JButton SEND = new JButton("Send");
    private static final JButton GET = new JButton("Get");

    public static void tryToSend() {
        String from = FROM_FIELD.getText();
        String to = TO_FIELD.getText();
        String regex = ".+@gmail\\.com";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(from);
        if (!m.matches())
            throw new IllegalArgumentException("Invalid email 1");
        regex = ".+@(\\w+\\.)([a-z]{2,4})";
        p = Pattern.compile(regex);
        m = p.matcher(to);
        if (!m.matches())
            throw new IllegalArgumentException("Invalid email 2");

        String pwd = new String(PASSWORD.getPassword());
        String thema = TOPIC_FIELD.getText();
        String text = MESSAGE_AREA.getText();
        EmailSend emailSend = new EmailSend();
        emailSend.sendEmail(from, from.substring(0, from.length() - 10), pwd, to, thema, text);
    }

    public static void tryToGet() {
        String from = FROM_FIELD.getText();
        String regex = ".+@gmail\\.com";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(from);
        if (!m.matches())
            throw new IllegalArgumentException("Invalid email 1");
        String pwd = new String(PASSWORD.getPassword());
        ReadEmail readEmail = new ReadEmail(from, pwd);
        GET_MESSAGE_AREA.setText(readEmail.getMessage());
    }

    public static void main(String[] args) throws InterruptedException, IOException {
        setUp();
    }

    private static void setUp() throws InterruptedException, IOException {
        MAIN_WINDOW.setSize(600, 600);

        Container pane = MAIN_WINDOW.getContentPane();
        pane.setLayout(new GridLayout(7, 5, 5, 5));
        FROM.setHorizontalAlignment(JLabel.RIGHT);
        pane.add(FROM);
        pane.add(FROM_FIELD);
        PASSWORD.setEchoChar('*');
        PASS.setHorizontalAlignment(JLabel.RIGHT);
        pane.add(PASS);
        pane.add(PASSWORD);
        TO.setHorizontalAlignment(JLabel.RIGHT);
        pane.add(TO);
        pane.add(TO_FIELD);
        MESSAGE.setHorizontalAlignment(JLabel.RIGHT);
        pane.add(MESSAGE);
        pane.add(MESSAGE_AREA);
        GET_MESSAGE.setHorizontalAlignment(JLabel.RIGHT);
        pane.add(GET_MESSAGE);
        pane.add(GET_MESSAGE_AREA);
        TOPIC.setHorizontalAlignment(JLabel.RIGHT);
        pane.add(TOPIC);
        pane.add(TOPIC_FIELD);

        SEND.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    tryToSend();
                } catch (IllegalArgumentException exp) {
                    if (exp.getMessage() == "Invalid email 1")
                        FROM_FIELD.setText("Invalid email");
                    else
                        TO_FIELD.setText("Invalid email");
                }
            }
        });
        GET.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tryToGet();
            }
        });
        pane.add(SEND);
        pane.add(GET);

        MAIN_WINDOW.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MAIN_WINDOW.setVisible(true);
    }
}
